//
//  MainView.swift
//  WeSplit
//
//  Created by Karla Pangilinan on 5/28/24.
//

import DiffableTextViews
import SwiftUI

struct MainView: View {
  @State private var checkAmount: Double = 0.0
  @State private var numberOfPeople: Int = 2
  @State private var tipPercentage: Int = 20

  private let tipPercentages: [Int] = [10, 15, 20, 25, 0]

  private var currencyCode: String {
    Locale.current.currency?.identifier ?? "USD"
  }

  private var totalPerPerson: Double {
    let peopleCount = Double(numberOfPeople + 2)
    let tipSelection = Double(tipPercentage)

    let tipValue = checkAmount / 100 * tipSelection
    let grandTotal = checkAmount + tipValue
    let amountPerPerson = grandTotal / peopleCount

    return amountPerPerson
  }

  var body: some View {
    NavigationView {
      Form {
        topSection
        tipPickerSection
        totalSection
      }
      .navigationTitle("WeSplit")
    }
  }
}

// MARK: - Views
extension MainView {
  private var topSection: some View {
    Section {
      DiffableTextField(value: $checkAmount) {
        .currency(code: currencyCode)
      }
      .environment(\.locale, Locale.current)
      .diffableTextViews_toolbarDoneButton()
      .diffableTextViews_keyboardType(.decimalPad)

      Picker("Number of People", selection: $numberOfPeople) {
        ForEach(2..<100) {
          Text(String($0))
        }
      }
    }
  }

  private var tipPickerSection: some View {
    Section {
      Picker("Tip Percentage", selection: $tipPercentage) {
        ForEach(tipPercentages, id: \.self) {
          Text("\($0)%") // Same format as the selected text displayed on picker
        }
      }
      .pickerStyle(.segmented)

    } header: {
      Text("How much tip do you want to leave?")
    }
  }

  private var totalSection: some View {
    Section {
      Text(totalPerPerson,
           format: .currency(code: currencyCode))
      .foregroundColor(tipPercentage == 0 ? .red : .black)
    }
  }
}


#Preview {
  MainView()
}
