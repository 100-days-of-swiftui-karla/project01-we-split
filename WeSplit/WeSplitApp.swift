//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Karla Pangilinan on 5/28/24.
//

import SwiftUI

@main
struct WeSplitApp: App {
  var body: some Scene {
    WindowGroup {
      MainView()
//      StudentSelectionView()
    }
  }
}
