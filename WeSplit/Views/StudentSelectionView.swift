//
//  StudentSelectionView.swift
//  WeSplit
//
//  Created by Karla Pangilinan on 5/29/24.
//

import SwiftUI

// MARK: - Model
struct Student: Identifiable, Hashable {
  var id: String {
    return String(idInt) + name
  }
  let idInt: Int
  let name: String
}

// MARK: - View
struct StudentSelectionView: View {
  @State private var tapCount: Int = 0
  @State private var textfieldText: String = ""
  @State private var selectedStudent: String = ""
  @State private var selectedStudent2 = ""
  @FocusState private var textfieldIsFocused: Bool

  let students = ["Harry", "Hermione", "Ron", "Cedric", "Malfoy"]

  let students2: [Student] = [.init(idInt: 1, name: "Harry"),
                              .init(idInt: 2, name: "Hermione"),
                              .init(idInt: 3, name: "Ron"),
                              .init(idInt: 4, name: "Cedric"),
                              .init(idInt: 5, name: "Malfoy")]

  var body: some View {
    NavigationView {
      Form {
        TextField("Your Name", text: $textfieldText)
          .keyboardType(.alphabet)
          .focused($textfieldIsFocused)

        Picker("Select your student", selection: $selectedStudent) {
          ForEach(students, id: \.self) {
            Text($0)
          }
        }

        Picker("Select your student 2", selection: $selectedStudent2) {
          ForEach(students2) {
            Text($0.name)
          }
        }
      }
      .toolbar {
        ToolbarItemGroup(placement: .keyboard) {
          Spacer()
          Button(action: {
            textfieldIsFocused = false
          }, label: {
            Image(systemName: "keyboard.chevron.compact.down")
          })
        }
      }
    }
  }
}


#Preview {
  StudentSelectionView()
}
